package com.company;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.Collections.max;
import static java.util.Collections.min;

public class Client {
    private Socket socket = null;
    private DataInputStream input = null;
    private DataOutputStream out = null;
    private Map<String, Quote> nbboMap = new HashMap<>();


    public Client(String address, int port) {
        // establish a connection
        try {
            socket = new Socket(address, port);
            System.out.println("Connected");
            InputStream input = socket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
            String line = "";

            while ((line = reader.readLine()) != null) {

                if (line.startsWith("Q|")) {
                    System.out.println(line);

                    Quote q = new Quote();
                    String[] vals = line.split(Pattern.quote("|"));
                    q.symbol = vals[1];
                    q.exchange = vals[2];
                    q.bid = Integer.parseInt(vals[3]);
                    q.offer = Integer.parseInt(vals[4]);
                    nbboMap.put(q.symbol + "|" + q.exchange, q);

                    printUpdatedNbboForSymbol(q.symbol);
                }

            }
        } catch (UnknownHostException u) {
            System.out.println(u);
        } catch (IOException i) {
            System.out.println(i);
        }

        // close the connection
        try {
            input.close();
            out.close();
            socket.close();
        } catch (IOException i) {
            System.out.println(i);
        }
    }

    private void printUpdatedNbboForSymbol(String symbol) {
        //DELL IBM, BATS, NASDAQ
        List<Integer> offerList = nbboMap.entrySet().stream()
                .filter(x -> x.getKey().startsWith(symbol))
                .map(x -> x.getValue().offer)
                .collect(Collectors.toList());
        List<Integer> bidist = nbboMap.entrySet().stream()
                .filter(x -> x.getKey().startsWith(symbol))
                .map(x -> x.getValue().bid)
                .collect(Collectors.toList());

        System.out.println("*** " + symbol + " NBBO = " + max(bidist) + " @ " + min(offerList));

    }

    class Quote {
        public String symbol;
        public String exchange;
        public int bid;
        public int offer;

    }

    public static void main(String args[]) {
        Client client = new Client("199.83.14.77", 7777);
    }
} 
